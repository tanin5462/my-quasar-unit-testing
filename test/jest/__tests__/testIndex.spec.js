import {
  mount,
  createLocalVue,
  shallowMount
} from '@vue/test-utils'
import myIndex from '../../../src/pages/Index.vue'
import * as All from 'quasar'
// import langEn from 'quasar/lang/en-us' // change to any language you wish! => this breaks wallaby :(
const {
  Quasar,
  date
} = All

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key]
  if (val && val.component && val.component.name != null) {
    object[key] = val
  }
  return object
}, {})


describe('My First Test', () => {
  const localVue = createLocalVue()
  localVue.use(Quasar, {
    components
  })

  const wrapper = mount(myIndex, {
    localVue
  }) // อ้างอิงถึง components 
  const vm = wrapper.vm // เข้าถึง ฟังก์ชัน หรือตัวแปลภายใน components ที่ mount ไว้จาก wrapper

  it("Will show div in page", () => {
    expect(wrapper.find("div").isVisible()).toBe(true)
  })

  it("Will show q-btn in page", () => {
    expect(wrapper.find("#myBtn").isVisible()).toBe(true)
  })

  it('เมื่อคลิกปุ่ม ค่า Result ต้องมีค่าเท่ากับ 10', async () => {
    wrapper.find("#myBtn").trigger('click')
    await wrapper.vm.$nextTick()
    const result = wrapper.find("#myResult").element.textContent
    expect(result).toBe("10")
  })

  it('เมื่อคลิกปุ่ม และค่ าanswer มีค่าเท่ากับ 100 ต้องแสดงผลลัพธ์ 100', async () => {
    vm.answer = "90"
    wrapper.find("#myBtn").trigger('click')
    await wrapper.vm.$nextTick()
    const result = wrapper.find("#myResult").element.textContent
    expect(result).toBe("100")
  })





})
